## Description
These are the custom config changes and additions needed for Apache2 to serve the Xymon Server webpages. It should not break any existing functionality you may have already had working through Apache.  

## Assumptions
You have installed Apache2 and the required Xymon Server packages.  
```
sudo apt install apache2 curl mrtg rrdtool librrds-perl xymon
```

## Instructions
1. Open apache2.conf  
```
sudo nano /etc/apache2/apache2.conf
```  

2. Edit these lines.  
These changes enable the symbolic url functions that Xymon needs and also the security features of Apache to work with _.htaccess_ and _.htpasswd_ files so you can secure your Xymon webpages if desired.
```
<Directory />
		Options FollowSymLinks
		AllowOverride All
		Require all granted
</Directory>

<Directory /usr/share>
		AllowOverride All
		Require all granted
</Directory>

<Directory /var/www/>
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
</Directory>

AccessFileName .htaccess

<FilesMatch "^\.ht">
		Require all granted
</FilesMatch>
```

3. Add this chunk at the **end** of the file. It won't work unless it's at the end.  
These changes connect Apache to the Xymon Server installation directories.  
```
#################################################################
## The following is for Xymon web aliases to function properly ##
#################################################################
Alias /xymon/  "/var/lib/xymon/www/"
<Directory "/var/lib/xymon/www">
    AllowOverride All
    Options Indexes FollowSymLinks Includes MultiViews
    Require all granted
</Directory>

ScriptAlias /xymon-cgi/ "/usr/lib/xymon/cgi-bin/"
<Directory "/usr/lib/xymon/cgi-bin">
    AllowOverride All
    Options ExecCGI Includes
    Require all granted
</Directory>

ScriptAlias /xymon-seccgi/ "/usr/lib/xymon/cgi-secure/"
<Directory "/usr/lib/xymon/cgi-secure">
    AllowOverride All
    Options ExecCGI Includes
    Require all granted
</Directory>
#######################################################
```

4. Save apache2.conf and exit nano.  

5. Restart apache2 to see if there are errors.  
```
sudo systemctl restart apache2.service
```

6. Test the default Xymon webpage. If it works, you're done with Apache.  
http://ServerIP/xymon/ 

7. Now you can set the [custom theme](https://gitlab.com/ionetworkadmin/xymon-server-custom-theme) that makes things pretty.


## License
Open Source

## Authors and acknowledgment
Kris Springer  
https://www.ionetworkadmin.com  
